<?php
/**
 * Gugliotti Helloworld
 */

/**
 * class Gugliotti_Helloworld_Block_Helloworld
 *
 * Hello World Block.
 * @author Andre Gugliotti <andre@gugliotti.com.br>
 * @version 0.1.0
 * @package CMS
 * @license GNU General Public License, version 3
 */
class Gugliotti_Helloworld_Block_Helloworld extends Mage_Core_Block_Template
{
	/**
	 * isEnabled
	 *
	 * Returns true if the module is enabled to be displayed.
	 * @return boolean
	 */
	public function isEnabled()
	{
		return Mage::helper('gugliotti_helloworld')->isEnabled();
	}

	/**
	 * getMessage
	 *
	 * Returns the custom message, if the module is enabled.
	 * @return string|boolean
	 */
	public function getMessage()
	{
		if ($this->isEnabled()) {
			return Mage::helper('gugliotti_helloworld')->getConfigData('configuration/custom_message');
		}
		return false;
	}

	/**
	 * getUrl
	 *
	 * Returns the Hello World URL.
	 * @return string
	 */
	public function getHelloworldUrl()
	{
		return Mage::getBaseUrl() . 'helloworld';
	}

	/**
	 * getMultipleMessages
	 *
	 * Returns the message or messages, splitted by comma.
	 * @return array
	 */
	public function getMultipleMessages()
	{
		// get raw messages
		$messagesRaw = Mage::helper('gugliotti_helloworld')->getConfigData('configuration/custom_message');

		// explode messages into array
		$messages = explode(',', $messagesRaw);
		return $messages;
	}
}
