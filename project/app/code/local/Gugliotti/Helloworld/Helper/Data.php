<?php
/**
 * Gugliotti Helloworld
 */

/**
 * class Gugliotti_Helloworld_Helper_Data
 *
 * Main helper.
 * @author Andre Gugliotti <andre@gugliotti.com.br>
 * @version 0.1.0
 * @package CMS
 * @license GNU General Public License, version 3
 */
class Gugliotti_Helloworld_Helper_Data extends Mage_Core_Helper_Abstract
{
	/**
	 * getConfigData
	 *
	 * Returns the value for the requested configuration.
	 * @param string $data
	 * @return mixed
	 */
	public function getConfigData($data)
	{
		return Mage::getStoreConfig('gugliotti_helloworld/' . $data);
	}

	/**
	 * isEnabled
	 *
	 * Returns true if the module is enabled to be displayed.
	 * @return boolean
	 */
	public function isEnabled()
	{
		return $this->getConfigData('configuration/enabled');
	}
}
