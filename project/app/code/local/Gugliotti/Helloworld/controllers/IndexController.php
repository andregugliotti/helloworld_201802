<?php
/**
 * Gugliotti Helloworld
 */

/**
 * class Gugliotti_Helloworld_IndexController
 *
 * Main Controller.
 * @author Andre Gugliotti <andre@gugliotti.com.br>
 * @version 0.1.0
 * @package CMS
 * @license GNU General Public License, version 3
 */
class Gugliotti_Helloworld_IndexController extends Mage_Core_Controller_Front_Action
{
	/**
	 * indexAction
	 */
	public function indexAction()
	{
		$this->loadLayout();
		$this->renderLayout();
	}
}
